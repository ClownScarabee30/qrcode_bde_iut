from __future__ import print_function

from email import encoders

from Mail import *
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import *

from Google import *
from deleter import *
import qrcode
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from googleapiclient.http import MediaFileUpload
import pandas as pd
# Voir le readme.txt en cas d'erreur ModuleNotFoundError:

#Documentation au sein du script
# état de fonctionnement : OK
# principe :
# -Crée une carte membre, comportant un numéro d'identifiant unique.
# -Uppload cette carte sur un google drive personnel (avec un dossier comportant ses infos) et générer un lien en lecture seul
# -Générer un qr code via le même numéro d'identifiant qui comporte le lien de la carte membre
# -Retrouver le mail étudiant via les infos fournit et envoyer un mail comportant le qrcode (phase béta, voir homonyme)
# -Update le fichier variable.txt afin de crée un nouvel identifiant unique



#Récupération info du membre
#demande au user de rentrer nom prénom date
#repasse les nom prénom en capitalize
#demande une confirmation avant de continuer
print("Qr_Bot ! \n Version: stable avec erreur sur token-pickle\n Owner : Noah GALLOIS")
rep = "no"

while rep != "yes":
    print("Création carte membre")
    nom = input("Entrez le nom du Membre : ")
    prenom = input("Entrez le prénom du Membre : ")
    nom = nom.lower()
    prenom = prenom.lower()
    nom = nom.capitalize()
    prenom = prenom.capitalize()
    date_nais = input("Entrez une date de naissance de type 'xx/xx/xx'")
    rep = input("Vous confirmez ces données : \n Nom: " + nom+"\n prenom : "+prenom+"\n date de naissance :"+date_nais+"\n repondre 'yes' pour confimer : ")
    rep = rep.lower()


#Counter
#extraction du numéro d'identifiant stocké dans le fichier variable .txt

f = open('Variable.txt', 'r')
if f.mode == 'r':
    contents = f.read()
contents = contents[10:]

#--------------------------------------
#FICHE MEMBRE CREATOR
#reprend une image par défaut pour y appliquer les informations dessus
#applique les données via des coordonées x y et une font

font = ImageFont.truetype("arial.ttf", 9) #modification police

img = Image.open('index_fiche.jpeg')

draw = ImageDraw.Draw(img)
draw.text((55, 28),nom,(0,0,0),font=font)
draw.text((75, 45),prenom,(0,0,0),font=font)
draw.text((55, 65),"N°"+contents,(0,0,0),font=font)

#sauvegarde de l'image avec son numéro d'identifiant
img_name ='Fiche N° '+contents+'.png'
img.save(img_name)

#------------------------

#CREATION DOSSIER + UPPLOAD SUR LE DRIVE
#Utilisation de l'Api de G-Drive pour crée un dossier
#le dossier contient les infos suivantes
#contents = numéro d'identification unique + le nom et prénom

CLIENT_SECRET_FILE = 'credentials.json' #info secrete client (actuellement pc noah bureau)
API_NAME = 'drive'
API_VERSION = 'v3'
SCOPES = ['https://www.googleapis.com/auth/drive']

service = Create_Service(CLIENT_SECRET_FILE,API_NAME,API_VERSION,SCOPES) #error possible
##appel au fichier token_drive_v3 si erreur supprimer le fichier(recréation au propre automatique)


print(dir(service))

folder = contents + "_" + prenom + "_" + nom
print(folder)
file_metadata = {
    'name': folder,
    'mimeType': 'application/vnd.google-apps.folder',
    'parents': ['1jV1nLXvff19EmHIQnYNOlJF2_P1UA0nt']
}
service.files().create(body=file_metadata).execute()

#UPLOAD FICHIER CARTE MEMBRE DRIVE
#le g drive fonctionne avec des id de dossiers chaque dossier possède une id unique indiquant son emplacement.

#récupération ID du dossier membre
#bon je viens de me rendre compte que l'api google propose une fonction pour récup immédiatement un id...
#cette partie est donc optimisable
#m'en fou ma fonction marche (uniquement dans le drive du LED)

id_parent = '1jV1nLXvff19EmHIQnYNOlJF2_P1UA0nt' #id du dossier dans lequel notre stockage de membre se trouve
name = folder #on reprend le nom du dossier que l'on vient de crée

query = f"parents = '{id_parent}' and name = '{name}'" #on génère une requete recherchant dans le dossier parent le dossier avec le nom name
response = service.files().list(q=query).execute()
files = response.get('files')
nextPageToken = response.get('nextPageToken')

while nextPageToken: #on récupère tout les dossiers répondant à cette requête (normalement on n'aura qu'une seule réponse)
    # c'est l'intéret de l'id unique
    response = service.files().list(q=query,pageToken=nextPageToken).execute()
    files.extend(response.get('files'))
    nextPageToken = response.get('nextPageToken')
# on génère un tableau comportant les informations pour chaque réponse à notre requete
pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 500)
pd.set_option('display.min_rows', 500)
pd.set_option('display.max_colwidth', 150)
pd.set_option('display.width', 200)
pd.set_option('expand_frame_repr', True)

df = pd.DataFrame(files)
#print(df)

id_membre = df.iloc[0,:].id #RECUP ID Dossier Membre
print("Id Dossier membre :")
print(id_membre)

#UPPLOAD CARTE MEMBRE DANS L'ID DOSSIER MEMBRE
#maintenant que l'on a récuperer l'id du dossier que l'on vient de crée (c'était le bordel) on peut upload des données dans celui-ci
#id_membre = id dossier membre
file_names = [img_name] #où img_name est le nom de l'image que l'on a générer précedement
mime_types = ['image/png']

for file_name,mime_type in zip(file_names,mime_types):
    file_metadata = {
        'name': file_name,
        'parents': [id_membre]
    }
    media = MediaFileUpload(file_name,mime_type)
    service.files().create(
        body=file_metadata,
        media_body=media,
        fields='id'
    ).execute()

#RECUPERATION ID CARTE MEMBRE DU DRIVE

id_parent = id_membre #on se place dans le dossier que l'on a générer précedement
name = img_name #où img_name est le nom de l'image que l'on vient de générer

query = f"parents = '{id_parent}' and name = '{name}'" #génération de la requête pour trouver l'id de l'image que l'on vient de crée (une réponse attendu)
response = service.files().list(q=query).execute()
files = response.get('files')
nextPageToken = response.get('nextPageToken')

while nextPageToken: #même procédure que pour trouver l'id de dossier
    response = service.files().list(q=query,pageToken=nextPageToken).execute()
    files.extend(response.get('files'))
    nextPageToken = response.get('nextPageToken')

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 500)
pd.set_option('display.min_rows', 500)
pd.set_option('display.max_colwidth', 150)
pd.set_option('display.width', 200)
pd.set_option('expand_frame_repr', True)

df = pd.DataFrame(files)
id_carte_membre = df.iloc[0,:].id #Recup ID Dossier Membre
print("Id carte membre : ")
print(id_carte_membre) #voici l'id de notre image

#GENERATION DU LIEN DE PARTAGE DE LA CARTE MEMBRE
#après avoir récupérer l'id de notre image (qui est unique) on va pouvoir lui générer un lien de partage publique et utilisable par tout le monde
#ce lien sera en lecture seulement
#id = id_carte-membre
requests_body = {
    'role': 'reader',
    'type': 'anyone'
}
response_permission = service.permissions().create(
    fileId=id_carte_membre,
    body=requests_body
).execute()



response_share_link = service.files().get(
    fileId=id_carte_membre,
    fields='webViewLink'
).execute()
print("Lien de partage de la carte membre : ")
print(response_share_link) #ce lien sera réutiliser plus tard


link = response_share_link['webViewLink']

#GENERATION DU QRCODE
#on vient de récuperer le lien de partage de notre image
#il nous suffit alors de simplement générer un qrcode comportant le lien générer
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=10,
    border=4,
)
qr.add_data(link) #Integration du lien
qr.make(fit=True)

img = qr.make_image(fill_color="blue", back_color="white")  #custom qrcode
type(img)  # qrcode.image.pil.PilImage

qr_name = "QR N° "+contents + ".png" #on donne un nom unique à notre qrcode générer (on compte pouvoir le retrouver facilement)
#on notera que lui aussi comporte le numéro d'identifiant unique (même que celui de la carte)
img.save(qr_name) #on le sauvegarde

#UPPLOAD DU QR CODE DANS LE DOSSIER ID MEMBRE
#même fonctionnement que pour l'uppload de l'image
#on connait déjà l'id du dossier que l'on a générer (trouvé précèdement)
#id_membre = id dossier membre
file_names = [qr_name]
mime_types = ['image/png'] #un qrcode est une image

for file_name,mime_type in zip(file_names,mime_types):
    file_metadata = {
        'name': file_name,
        'parents': [id_membre]
    }
    media = MediaFileUpload(file_name,mime_type)
    service.files().create(
        body=file_metadata,
        media_body=media,
        fields='id'
    ).execute()

#ENVOIE DU MAIL AVEC LE QR CODE AU MEMBRE
#on retrouve l'adresse mail étudiante
prenom = prenom.lower()
nom = nom.lower()
mail_membre = prenom+"."+nom+"@etudiant.univ-reims.fr"

#generer mail via API GMail
#Si error après cette partie, delete le token_gmail.pickle (regen automatique)
CLIENT_SECRET_FILE = "credentials_gmail.json"
API_NAME = 'gmail'
API_VERSION = 'v1'
SCOPES = ['https://mail.google.com/']

service = Create_Service(CLIENT_SECRET_FILE,API_NAME,API_VERSION,SCOPES)
#appel au fichier token_gmail_v1 si erreur supprimer le fichier(recréation au propre automatique)

file_attachments = [qr_name]
prenom = prenom.capitalize()

body = "Salut "+prenom+" !\nBienvenue au BDE du LED, si tu reçoit se mail, c'est que tu viens probablement de crée ta carte membre \nEt la voici déjà prête !\n"
"\nGrâce à ce petit Qr_code, de tas d'avantages s'offre à toi ! Mais comment sa marche ?"
"Eh bien derrière tout se petit mail se cache un gentil bot crée par la secrétaire qui se charge de t'envoyer un qrcode lié à ta carte membre dématérialisé\n"
"La seul chose que tu as à faire, c'est de garder ce petit Qr_code bien au chaud dans ton téléphone, et avant chaque évènement," \
"tu n'auras qu'a le dégainer pour montrer que tu fais bien partie du LED ! Simple comme un Pass Sanitaire héhé..." \
"\n\nL'équipe LED te remercie de ton adhésion et te souhaite une meirveilleuse année scolaire à ces côtés !"
mimeMessage = MIMEMultipart()
mimeMessage['to'] = mail_membre
mimeMessage['subject'] = 'Ton Qr_Code de Membre LED !'
mimeMessage.attach(MIMEText(body, 'plain')) #html possible

for attachment in file_attachments:
    contents_type, encoding = mimetypes.guess_type(attachment)
    main_type, sub_type = contents_type.split('/',1)

    file_name = os.path.basename(attachment)

    f = open(attachment, 'rb')

    myFile = MIMEBase(main_type, sub_type)
    myFile.set_payload(f.read())
    myFile.add_header('Content-Disposition','attachment', filename=file_name)
    encoders.encode_base64(myFile)

    f.close()
    mimeMessage.attach(myFile)

raw_string = base64.urlsafe_b64encode(mimeMessage.as_bytes()).decode()

message = service.users().messages().send( #erreur possible sol : delete token gmail
    userId='me',
    body={'raw': raw_string}).execute()

print(message)
#https://www.youtube.com/watch?v=29eJb5IIBDs
#8:16

#MODIFICATION DU FICHIER VARIABLE
count = int(contents)
count += 1
file1 = open("Variable.txt", "w")
str1 = str(count)
file1.write("counter = " + str1)
file1.close()

#SUPPRESSION DES FICHIERS LOCAUX
delete()
#ATTENTION une erreur est générer lors de la suppression des fichiers locaux sous windows
#de manière à pouvoir conserver le dernier qr code crée.
#celui-ci sera supprimé lorsque le programme sera réutiliser